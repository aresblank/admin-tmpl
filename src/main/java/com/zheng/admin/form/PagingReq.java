package com.zheng.admin.form;

import java.util.List;

/**
 * Created by zcz on 2017/10/31.
 */
public class PagingReq {

    private Integer draw;

    private Integer length;

    private Long start;

    private List<PagingColumn> columns;

    private PagingSearch search;

    private List<PagingOrder> order;

    public Integer getDraw() {
        return draw;
    }

    public void setDraw(Integer draw) {
        this.draw = draw;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public List<PagingColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<PagingColumn> columns) {
        this.columns = columns;
    }

    public PagingSearch getSearch() {
        return search;
    }

    public void setSearch(PagingSearch search) {
        this.search = search;
    }

    public List<PagingOrder> getOrder() {
        return order;
    }

    public void setOrder(List<PagingOrder> order) {
        this.order = order;
    }
}
