package com.zheng.admin.form;

/**
 * Created by zcz on 2017/10/31.
 */
public class PagingOrder {

    private String column;

    private String dir;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }
}
