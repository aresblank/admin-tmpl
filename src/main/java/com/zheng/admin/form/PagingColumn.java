package com.zheng.admin.form;

/**
 * Created by zcz on 2017/10/31.
 */
public class PagingColumn {

    private String name;

    private String data;

    private Boolean searchable;

    private Boolean orderable;

    private PagingSearch search;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Boolean getSearchable() {
        return searchable;
    }

    public void setSearchable(Boolean searchable) {
        this.searchable = searchable;
    }

    public Boolean getOrderable() {
        return orderable;
    }

    public void setOrderable(Boolean orderable) {
        this.orderable = orderable;
    }

    public PagingSearch getSearch() {
        return search;
    }

    public void setSearch(PagingSearch search) {
        this.search = search;
    }
}
