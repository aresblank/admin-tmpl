package com.zheng.admin.form;

/**
 * Created by zcz on 2017/10/27.
 */
public class LoginUser {

    private String phone;

    private String password;

    private Boolean rememberMe;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(Boolean rememberMe) {
        this.rememberMe = rememberMe;
    }
}
