package com.zheng.admin.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zcz on 2017/10/27.
 */
public class AdminResp {

    private String code;

    private String message;

    private Map<String, String> data = new HashMap<String, String>();

    public AdminResp(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static AdminResp SUCCESS_RESP = new AdminResp("success", "成功");
    public static AdminResp FAIL_RESP = new AdminResp("fail", "失败");

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public void addData(String key, String value) {
        if (data == null) {
            data = new HashMap<String, String>();
        }
        data.put(key, value);
    }
}
