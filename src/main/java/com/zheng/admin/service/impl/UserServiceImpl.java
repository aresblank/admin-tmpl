package com.zheng.admin.service.impl;

import com.zheng.admin.entity.PermSubject;
import com.zheng.admin.form.PagingReq;
import com.zheng.admin.form.PagingResp;
import com.zheng.admin.mapper.UserMapper;
import com.zheng.admin.model.Perm;
import com.zheng.admin.model.Role;
import com.zheng.admin.model.User;
import com.zheng.admin.service.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by zcz on 2017/10/27.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUserByPhone(String phone) {
        return userMapper.getUserByPhone(phone);
    }

    @Override
    public Integer createUser(User user) {
        return userMapper.insertUser(user);
    }

    @Override
    public Integer updateUser(User user) {
        return userMapper.updateUser(user);
    }

    @Override
    public Integer createRole(Role role) {
        return userMapper.insertRole(role);
    }

    @Override
    public Integer updateRole(Role role) {
        return userMapper.updateRole(role);
    }

    @Override
    public Integer createPerm(Perm perm) {
        return userMapper.insertPerm(perm);
    }

    @Override
    public Integer updatePerm(Perm perm) {
        return userMapper.updatePerm(perm);
    }

    @Override
    public Integer deleteRoleByIds(Integer... ids) {
        return userMapper.deleteRoles(ids);
    }

    @Override
    public Integer deletePermByIds(Integer... ids) {
        return userMapper.deletePerms(ids);
    }

    @Override
    public User getUserById(Integer id) {
        return userMapper.getUserById(id);
    }

    @Override
    public Integer deleteUserByIds(Integer... ids) {
        return userMapper.deleteUsers(ids);
    }

    @Override
    public Integer deleteUserRole(Integer userId, Integer roleId) {
        return userMapper.deleteUserRole(userId, roleId);
    }

    @Override
    public Integer addUserRole(Integer userId, Integer roleId) {
        return userMapper.addUserRole(userId, roleId);
    }

    @Override
    public Role getRoleById(Integer id) {
        return userMapper.getRoleById(id);
    }

    @Override
    public List<Role> queryAllRoles() {
        return userMapper.getAllRoles();
    }

    @Override
    public List<Role> queryUserRoles(Integer userId) {
        List<Role> roleList = userMapper.getUserRoleByUserId(userId);
        return roleList;
    }

    @Override
    public PermSubject buildUserPermSubject(Integer userId) {
        PermSubject permSubject = new PermSubject();
        List<Perm> perms = userMapper.findPerms(userId);
        if (CollectionUtils.isNotEmpty(perms)) {
            for (Perm perm : perms) {
                permSubject.addPerm(perm);
            }
        }
        return permSubject;
    }

    @Override
    public PagingResp<User> queryUserByPaging(PagingReq req) {
        List<User> users = userMapper.queryUserByPaging(req);
        Long count = userMapper.queryUserCountByPaging(req);
        PagingResp<User> resp = new PagingResp<User>();
        resp.setData(users);
        resp.setDraw(req.getDraw());
        resp.setRecordsTotal(count);
        resp.setRecordsFiltered(count);
        return resp;
    }

    @Override
    public List<Role> queryRoleByUserId(Integer userId) {
        return null;
    }

    @Override
    public PagingResp<Role> queryRoleByPaging(PagingReq req) {
        List<Role> perms = userMapper.queryRoleByPaging(req);
        Long count = userMapper.queryRoleCountByPaging(req);
        PagingResp<Role> resp = new PagingResp<Role>();
        resp.setData(perms);
        resp.setDraw(req.getDraw());
        resp.setRecordsTotal(count);
        resp.setRecordsFiltered(count);
        return resp;
    }

    @Override
    public Perm getPermById(Integer id) {
        return userMapper.getPermById(id);
    }

    @Override
    public PagingResp<Perm> queryPermByPaging(PagingReq req) {
        List<Perm> perms = userMapper.queryPermByPaging(req);
        Long count = userMapper.queryPermCountByPaging(req);
        PagingResp<Perm> resp = new PagingResp<Perm>();
        resp.setData(perms);
        resp.setDraw(req.getDraw());
        resp.setRecordsTotal(count);
        resp.setRecordsFiltered(count);
        return resp;
    }

    @Override
    public PagingResp<Perm> queryRolePermByPaging(Integer roleId, PagingReq req) {
        List<Perm> perms = userMapper.queryRolePermByPaging(roleId, req);
        Long count = userMapper.queryRolePermCountByPaging(roleId, req);
        PagingResp<Perm> resp = new PagingResp<Perm>();
        resp.setData(perms);
        resp.setDraw(req.getDraw());
        resp.setRecordsTotal(count);
        resp.setRecordsFiltered(count);
        return resp;
    }

    @Override
    public PagingResp<Role> queryPermRoleByPaging(Integer permId, PagingReq req) {
        List<Role> perms = userMapper.queryPermRoleByPaging(permId, req);
        Long count = userMapper.queryPermRoleCountByPaging(permId, req);
        PagingResp<Role> resp = new PagingResp<Role>();
        resp.setData(perms);
        resp.setDraw(req.getDraw());
        resp.setRecordsTotal(count);
        resp.setRecordsFiltered(count);
        return resp;
    }

    @Override
    public PagingResp<Perm> queryUserPermByPaging(Integer userId, PagingReq req) {
        List<Perm> perms = userMapper.queryUserPermByPaging(userId, req);
        Long count = userMapper.queryUserPermCountByPaging(userId, req);
        PagingResp<Perm> resp = new PagingResp<Perm>();
        resp.setData(perms);
        resp.setDraw(req.getDraw());
        resp.setRecordsTotal(count);
        resp.setRecordsFiltered(count);
        return resp;
    }

    @Override
    public Integer deleteRolePerm(Integer permId, Integer roleId) {
        return userMapper.deleteRolePerm(permId, roleId);
    }

    @Override
    public Integer addRolePerm(Integer permId, Integer roleId) {
        return userMapper.addRolePerm(permId, roleId);
    }

    @Override
    public List<Integer> queryRolePermIds(Integer roleId) {
        return userMapper.queryRolePermIds(roleId);
    }

    @Override
    public List<Integer> queryPermRoleIds(Integer permId) {
        return userMapper.queryPermRoleIds(permId);
    }

    @Override
    public PagingResp<User> queryRoleUserByPaging(Integer roleId, PagingReq req) {
        List<User> perms = userMapper.queryRoleUserByPaging(roleId, req);
        Long count = userMapper.querRoleUserCountByPaging(roleId, req);
        PagingResp<User> resp = new PagingResp<User>();
        resp.setData(perms);
        resp.setDraw(req.getDraw());
        resp.setRecordsTotal(count);
        resp.setRecordsFiltered(count);
        return resp;
    }

    @Override
    public List<Integer> queryRoleUserIds(Integer roleId) {
        return userMapper.queryRoleUserIds(roleId);
    }
}
