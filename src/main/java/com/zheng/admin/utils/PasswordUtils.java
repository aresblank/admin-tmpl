package com.zheng.admin.utils;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import java.nio.charset.Charset;

/**
 * Created by zcz on 2017/10/27.
 */
public class PasswordUtils {

    private PasswordUtils() {}

    public static String encryptPassword(String source) {
        HashCode hashCode = Hashing.sha256().hashString(source, Charset.defaultCharset());
        return hashCode.toString();
    }

    public static boolean checkPass(String source, String encrypt) {
        return encryptPassword(source).equals(encrypt);
    }

    public static void main(String[] args) {
        System.out.println(Hashing.sha256().hashString("123456", Charset.defaultCharset()));
    }
}
