package com.zheng.admin.exception;

/**
 * Created by zcz on 2017/11/6.
 */
public class AdminSystemException extends RuntimeException {

    protected String code;
    protected String message;

    public AdminSystemException(String message) {
        super(message);
        this.message = message;
    }

    public AdminSystemException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }

    public AdminSystemException(String code, String message) {
        super("[" + code + "]" + message);
        this.code = code;
        this.message = message;
    }

    public AdminSystemException(String code, String message, Throwable throwable) {
        super("[" + code + "]" + message, throwable);
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
