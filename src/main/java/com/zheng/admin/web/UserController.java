package com.zheng.admin.web;

import com.zheng.admin.dto.AdminResp;
import com.zheng.admin.form.PagingReq;
import com.zheng.admin.form.PagingResp;
import com.zheng.admin.model.Perm;
import com.zheng.admin.model.Role;
import com.zheng.admin.model.User;
import com.zheng.admin.service.UserService;
import com.zheng.admin.utils.PasswordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by zcz on 2017/10/27.
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/user.html")
    public String userList(Model model) {
        return "user/userList";
    }

    @RequestMapping(value = "/user/{id}.html")
    public String userEdit(@PathVariable("id")Integer id, Model model) {
        User user = userService.getUserById(id);
        model.addAttribute("user", user);
        List<Role> roles = userService.queryUserRoles(id);
        model.addAttribute("roles", roles);

        List<Role> allRoles = userService.queryAllRoles();
        model.addAttribute("allRoles", allRoles);
        return "user/userEdit";
    }

    @ResponseBody
    @RequestMapping(value = "/user/{userId}/perm", method = RequestMethod.POST)
    public ResponseEntity<PagingResp<Perm>> queryUserPermByPaging(@PathVariable Integer userId, @RequestBody PagingReq req) {
        PagingResp<Perm> resp = userService.queryUserPermByPaging(userId ,req);
        return new ResponseEntity(resp, null, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/role/{roleId}/perm", method = RequestMethod.POST)
    public ResponseEntity<PagingResp<Perm>> queryRolePermByPaging(@PathVariable Integer roleId, @RequestBody PagingReq req) {
        PagingResp<Perm> resp = userService.queryRolePermByPaging(roleId ,req);
        return new ResponseEntity(resp, null, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/role/{roleId}/user", method = RequestMethod.POST)
    public ResponseEntity<PagingResp<Perm>> queryRoleUserByPaging(@PathVariable Integer roleId, @RequestBody PagingReq req) {
        PagingResp<User> resp = userService.queryRoleUserByPaging(roleId ,req);
        return new ResponseEntity(resp, null, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/perm/{permId}/role", method = RequestMethod.POST)
    public ResponseEntity<PagingResp<Role>> queryPermRoleByPaging(@PathVariable Integer permId, @RequestBody PagingReq req) {
        PagingResp<Role> resp = userService.queryPermRoleByPaging(permId ,req);
        return new ResponseEntity(resp, null, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public ResponseEntity<PagingResp<User>> queryUserByPaging(@RequestBody PagingReq req) {
        PagingResp<User> resp = userService.queryUserByPaging(req);
        return new ResponseEntity(resp, null, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/user/save", method = RequestMethod.POST)
    public AdminResp saveUser(@RequestBody User user) {
        try {
            //新建用户
            if (user.getId() == null) {
                user.setPassword(PasswordUtils.encryptPassword(user.getPassword()));
                user.setStatus("normal");
                Integer num = userService.createUser(user);
                if (num != null && num.intValue() == 1) {
                    AdminResp resp = new AdminResp("success", "创建用户成功");
                    resp.addData("userId", user.getId() + "");
                    return resp;
                } else {
                    return AdminResp.FAIL_RESP;
                }
            }
            //修改用户
            else {
                Integer num = userService.updateUser(user);
                if (num != null && num.intValue() == 1) {
                    AdminResp resp = new AdminResp("success", "修改用户成功");
                    resp.addData("userId", user.getId() + "");
                    return resp;
                } else {
                    return AdminResp.FAIL_RESP;
                }
            }
        } catch (Exception e) {
            return AdminResp.FAIL_RESP;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/user/delete", method = RequestMethod.POST)
    public AdminResp deleteUsers(@RequestBody Integer[] ids) {
        try {
            Integer num = userService.deleteUserByIds(ids);
            if (num > 0) {
                return AdminResp.SUCCESS_RESP;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AdminResp.FAIL_RESP;
    }

    @ResponseBody
    @RequestMapping(value = "/user/deleteUserRole", method = RequestMethod.POST)
    public AdminResp deleteUserRole(@RequestParam("userId") Integer userId, @RequestParam("roleId") Integer roleId) {
        try {
            Integer num = userService.deleteUserRole(userId, roleId);
            if (num > 0) {
                return AdminResp.SUCCESS_RESP;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AdminResp.FAIL_RESP;
    }

    @ResponseBody
    @RequestMapping(value = "/user/addUserRole", method = RequestMethod.POST)
    public AdminResp addUserRole(@RequestParam("userId") Integer userId, @RequestParam("roleId") Integer roleId) {
        try {
            Integer num = userService.addUserRole(userId, roleId);
            if (num > 0) {
                return AdminResp.SUCCESS_RESP;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AdminResp.FAIL_RESP;
    }

    @RequestMapping(value = "/role.html")
    public String roleList(Model model) {
        return "user/roleList";
    }

    @RequestMapping(value = "/role/{id}.html")
    public String roleEdit(@PathVariable("id")Integer id, Model model) {
        Role role = userService.getRoleById(id);
        model.addAttribute("role", role);
        return "user/roleEdit";
    }

    @ResponseBody
    @RequestMapping(value = "/role", method = RequestMethod.POST)
    public ResponseEntity<PagingResp<Role>> queryRoleByPaging(@RequestBody PagingReq req) {
        PagingResp<Role> resp = userService.queryRoleByPaging(req);
        return new ResponseEntity(resp, null, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/role/save", method = RequestMethod.POST)
    public AdminResp saveRole(@RequestBody Role role) {
        try {
            //新建角色
            if (role.getId() == null) {
                role.setStatus("normal");
                Integer num = userService.createRole(role);
                if (num != null && num.intValue() == 1) {
                    AdminResp resp = new AdminResp("success", "创建角色成功");
                    resp.addData("roleId", role.getId() + "");
                    return resp;
                } else {
                    return AdminResp.FAIL_RESP;
                }
            }
            //修改用户
            else {
                Integer num = userService.updateRole(role);
                if (num != null && num.intValue() == 1) {
                    AdminResp resp = new AdminResp("success", "修改角色成功");
                    resp.addData("roleId", role.getId() + "");
                    return resp;
                } else {
                    return AdminResp.FAIL_RESP;
                }
            }
        } catch (Exception e) {
            return AdminResp.FAIL_RESP;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/role/delete", method = RequestMethod.POST)
    public AdminResp deleteRole(@RequestBody Integer[] ids) {
        try {
            Integer num = userService.deleteRoleByIds(ids);
            if (num > 0) {
                return AdminResp.SUCCESS_RESP;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AdminResp.FAIL_RESP;
    }

    @ResponseBody
    @RequestMapping(value = "/role/queryPermRoleIds", method = RequestMethod.POST)
    public ResponseEntity<List<Integer>> queryPermRoleIds(@RequestParam("permId") Integer permId) {
        List<Integer> roleIds = userService.queryPermRoleIds(permId);
        return new ResponseEntity(roleIds, null, HttpStatus.OK);
    }


    @ResponseBody
    @RequestMapping(value = "/role/queryRoleUserIds", method = RequestMethod.POST)
    public ResponseEntity<List<Integer>> queryRoleUserIds(@RequestParam("roleId") Integer roleId) {
        List<Integer> roleIds = userService.queryRoleUserIds(roleId);
        return new ResponseEntity(roleIds, null, HttpStatus.OK);
    }

    @RequestMapping(value = "/perm.html")
    public String permList(Model model) {
        return "/user/permList";
    }

    @RequestMapping(value = "/perm/{id}.html")
    public String permEdit(@PathVariable("id")Integer id, Model model) {
        Perm perm = userService.getPermById(id);
        model.addAttribute("perm", perm);
        return "user/permEdit";
    }

    @ResponseBody
    @RequestMapping(value = "/perm", method = RequestMethod.POST)
    public ResponseEntity<PagingResp<User>> queryPermByPaging(@RequestBody PagingReq req) {
        PagingResp<Perm> resp = userService.queryPermByPaging(req);
        return new ResponseEntity(resp, null, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/role/deleteRolePerm", method = RequestMethod.POST)
    public AdminResp deleteRolePerm(@RequestParam("permId") Integer permId, @RequestParam("roleId") Integer roleId) {
        try {
            Integer num = userService.deleteRolePerm(permId, roleId);
            if (num > 0) {
                return AdminResp.SUCCESS_RESP;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AdminResp.FAIL_RESP;
    }

    @ResponseBody
    @RequestMapping(value = "/role/addRolePerm", method = RequestMethod.POST)
    public AdminResp addRolePerm(@RequestParam("permId") Integer permId, @RequestParam("roleId") Integer roleId) {
        try {
            Integer num = userService.addRolePerm(permId, roleId);
            if (num > 0) {
                return AdminResp.SUCCESS_RESP;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AdminResp.FAIL_RESP;
    }

    @ResponseBody
    @RequestMapping(value = "/perm/queryRolePermIds", method = RequestMethod.POST)
    public ResponseEntity<List<Integer>> queryRolePermIds(@RequestParam("roleId") Integer roleId) {
        List<Integer> permIds = userService.queryRolePermIds(roleId);
        return new ResponseEntity(permIds, null, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/perm/save", method = RequestMethod.POST)
    public AdminResp savePerm(@RequestBody Perm perm) {
        try {
            //新建用户
            if (perm.getId() == null) {
                perm.setStatus("normal");
                Integer num = userService.createPerm(perm);
                if (num != null && num.intValue() == 1) {
                    AdminResp resp = new AdminResp("success", "创建权限成功");
                    resp.addData("permId", perm.getId() + "");
                    return resp;
                } else {
                    return AdminResp.FAIL_RESP;
                }
            }
            //修改用户
            else {
                Integer num = userService.updatePerm(perm);
                if (num != null && num.intValue() == 1) {
                    AdminResp resp = new AdminResp("success", "修改权限成功");
                    resp.addData("permIdpermId", perm.getId() + "");
                    return resp;
                } else {
                    return AdminResp.FAIL_RESP;
                }
            }
        } catch (Exception e) {
            return AdminResp.FAIL_RESP;
        }
    }

    @ResponseBody
    @RequestMapping(value = "/perm/delete", method = RequestMethod.POST)
    public AdminResp deletePerm(@RequestBody Integer[] ids) {
        try {
            Integer num = userService.deletePermByIds(ids);
            if (num > 0) {
                return AdminResp.SUCCESS_RESP;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return AdminResp.FAIL_RESP;
    }
}
