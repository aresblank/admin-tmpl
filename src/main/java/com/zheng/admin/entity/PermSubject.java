package com.zheng.admin.entity;


import com.zheng.admin.model.Perm;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zcz on 16-3-15.
 */
public class PermSubject {

    private List<Perm> Perms = new ArrayList<Perm>();

    public boolean isPermitted(String PermName) {
        for (Perm Perm : Perms) {
            if (Perm.isPermitted(PermName)) {
                return true;
            }
        }
        return false;
    }

    public boolean[] isPermitted(String... PermNames) {
        if (PermNames == null || PermNames.length == 0) {
            return new boolean[0];
        }
        boolean[] result = new boolean[PermNames.length];
        for (int i = 0; i < PermNames.length; i++) {
            result[i] = isPermitted(PermNames[i]);
        }
        return result;
    }

    public List<Perm> getPerms() {
        return Perms;
    }

    public void setPerms(List<Perm> Perms) {
        this.Perms = Perms;
        for (Perm perm : Perms) {
            perm.initParts();
        }
    }

    public void addPerm(Perm Perm) {
        if (Perms == null) {
            Perms = new ArrayList<Perm>();
        }
        Perms.add(Perm);
        Perm.initParts();
    }
}
