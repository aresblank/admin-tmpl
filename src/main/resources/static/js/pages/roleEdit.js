$.App.Page.RolePermList = {
    "columns" : [
        { "data": "id", "title": "ID", "orderable": false  },
        { "data": "type", "title": "类型", "orderable": false },
        { "data": "name", "title": "名称", "orderable": false},
        { "data": "info", "title": "描述", "orderable": false },
        { "data": "opt", "title": "操作", "orderable": false }
    ],

    "columnDefs" : [
        {
            "targets": [-1], // 目标列位置，下标从0开始
            'className': 'dt-center',
            "width" : 80,
            "render": function(data, type, row) { // 返回自定义内容
               return '<a class="btn btn-xs btn-info" href="#/perm/' + row.id + '.html">编辑</a>' +
                      '<a class="btn btn-xs btn-danger btn-row-remove" href="javascript:void(0);" data-id="' + row.id +'">删除</a>';
            }
        }
    ],

    "drawCallback" : function(settings) {

    }
};

$.App.Page.RoleUserList = {
    "columns" : [
        { "data": "id", "title": "ID", "orderable": true  },
        { "data": "phone", "title": "手机号", "orderable": true },
        { "data": "email", "title": "邮箱", "orderable": false},
        { "data": "name", "title": "姓名", "orderable": false },
        { "data": "status", "title": "状态", "orderable": false },
        { "data": "opt", "title": "操作", "orderable": false }
    ],

    "columnDefs" : [
        {
            "targets": [-1], // 目标列位置，下标从0开始
            'className': 'dt-center',
            "width" : 80,
            "render": function(data, type, row) { // 返回自定义内容
               return '<a class="btn btn-xs btn-info" href="#/role/' + row.id + '.html">编辑</a>' +
                      '<a class="btn btn-xs btn-danger btn-row-remove" href="javascript:void(0);" data-id="' + row.id +'">删除</a>';
            }
        }
    ],

    "drawCallback" : function(settings) {

    }
};

$.App.Page.RolePermSelectList = {
    "columns" : [
        { "data": "id", "title": "ID", "orderable": false  },
        { "data": "type", "title": "类型", "orderable": false },
        { "data": "name", "title": "名称", "orderable": false},
        { "data": "info", "title": "描述", "orderable": false },
        { "data": "opt", "title": "选择", "orderable": false }

    ],

    "columnDefs" : [
        {
            "targets": [-1], // 目标列位置，下标从0开始
            'className': 'dt-center',
            "width" : 30,
            "render": function(data, type, row) { // 返回自定义内容
               return '<input type="checkbox" name="rp-row-chk" data-id="' + row.id + '"/>';
            }
        }
    ],
    "drawCallback" : function(settings) {
        $('#RolePermSelectList input[type=checkbox]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '10%'
        });
        var roleId = $("#id").val();
        $.ajax({
            url: '/perm/queryRolePermIds?roleId=' + roleId,
            type:"POST",
            contentType: "application/json",
            async : false,
            success : function(data){
                $.each(data, function( index, value ) {
                    $("#RolePermSelectList input[type=checkbox][data-id=" + value +"]").iCheck("check");
                });
            },
            error : function(){
                $.App.notify("连接失败,请稍候再试!", {type: "error"});
            }
        });
    }
};

$.App.Page.RoleUserSelectList = {
    "columns" : [
        { "data": "id", "title": "ID", "orderable": true  },
        { "data": "phone", "title": "手机号", "orderable": true },
        { "data": "email", "title": "邮箱", "orderable": false},
        { "data": "name", "title": "姓名", "orderable": false },
        { "data": "status", "title": "状态", "orderable": false },
        { "data": "opt", "title": "选择", "orderable": false }
    ],

    "columnDefs" : [
        {
            "targets": [-1], // 目标列位置，下标从0开始
            'className': 'dt-center',
            "width" : 30,
            "render": function(data, type, row) { // 返回自定义内容
               return '<input type="checkbox" name="rp-row-chk" data-id="' + row.id + '"/>';
            }
        }
    ],
    "drawCallback" : function(settings) {
        $('#RoleUserSelectList input[type=checkbox]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '10%'
        });
        var roleId = $("#id").val();
        $.ajax({
            url: '/role/queryRoleUserIds?roleId=' + roleId,
            type:"POST",
            contentType: "application/json",
            async : false,
            success : function(data){
                $.each(data, function( index, value ) {
                    $("#RoleUserSelectList input[type=checkbox][data-id=" + value +"]").iCheck("check");
                });
            },
            error : function(){
                $.App.notify("连接失败,请稍候再试!", {type: "error"});
            }
        });
    }
};

$(document).ready(function() {
    $(".add_perm_btn").on("click", function() {
        $("#addPermModal").modal("show");
    });
    $("#addPermModal").on("hidden.bs.modal", function(e){
        $.App.Page['RolePermList'].draw();
    });
    $("#addPermModal").on("show.bs.modal", function(e){
        $.App.Page['RolePermSelectList'].draw();
    });
    $("#addPermModal").on("ifClicked", "input[type=checkbox][name=rp-row-chk]", function(event) {
        var self = $(this);
        var isChecked = self.prop("checked");
        var roleId = $("#id").val();
        var permId = self.data("id");
        var url = "";
        if (isChecked) {
            url = "/role/deleteRolePerm?permId=" + permId + "&roleId=" + roleId;
        } else {
             url = "/role/addRolePerm?permId=" + permId + "&roleId=" + roleId;
        }
        $.ajax({
            url: url,
            type:"POST",
            contentType: "application/json",
            async : false,
            success : function(resp){
                if ('success' === resp.code) {
                    $.App.notify(resp.message);
                } else {
                    setTimeout(function(){
                        self.iCheck(isChecked ? "check" : "uncheck");
                    }, 200);
                    $.App.notify(resp.message, {type: "error"});
                }
            },
            error : function(){
                setTimeout(function(){
                        self.iCheck(isChecked ? "check" : "uncheck");
                }, 200);
                $.App.notify("连接失败,请稍候再试!", {type: "error"});
            }
        });
    });
    $("#RolePermList").on("click", ".btn-row-remove", function() {
        var roleId = $("#id").val();
        var permId = $(this).data("id");
        $.ajax({
            url: "/role/deleteRolePerm?permId=" + permId + "&roleId=" + roleId,
            type:"POST",
            contentType: "application/json",
            async : false,
            success : function(resp){
                if ('success' === resp.code) {
                    $.App.notify(resp.message);
                    $.App.Page['RolePermList'].draw();
                } else {
                    $.App.notify(resp.message, {type: "error"});
                }
            },
            error : function(){
                $.App.notify("连接失败,请稍候再试!", {type: "error"});
            }
        });
    });

    $(".add_user_btn").on("click", function() {
        $("#addUserModal").modal("show");
    });
    $("#addUserModal").on("hidden.bs.modal", function(e){
        $.App.Page['RoleUserList'].draw();
    });
    $("#addUserModal").on("show.bs.modal", function(e){
        $.App.Page['RoleUserSelectList'].draw();
    });
    $("#addUserModal").on("ifClicked", "input[type=checkbox][name=rp-row-chk]", function(event) {
        var self = $(this);
        var isChecked = self.prop("checked");
        var roleId = $("#id").val();
        var userId = self.data("id");
        var url = "";
        if (isChecked) {
            url = "/user/deleteUserRole?userId=" + userId + "&roleId=" + roleId;
        } else {
             url = "/user/addUserRole?userId=" + userId + "&roleId=" + roleId;
        }
        $.ajax({
            url: url,
            type:"POST",
            contentType: "application/json",
            async : false,
            success : function(resp){
                if ('success' === resp.code) {
                    $.App.notify(resp.message);
                } else {
                    setTimeout(function(){
                        self.iCheck(isChecked ? "check" : "uncheck");
                    }, 200);
                    $.App.notify(resp.message, {type: "error"});
                }
            },
            error : function(){
                setTimeout(function(){
                        self.iCheck(isChecked ? "check" : "uncheck");
                }, 200);
                $.App.notify("连接失败,请稍候再试!", {type: "error"});
            }
        });
    });

    $("#RoleUserList").on("click", ".btn-row-remove", function() {
        var roleId = $("#id").val();
        var userId = $(this).data("id");
        $.ajax({
            url: "/user/deleteUserRole?userId=" + userId + "&roleId=" + roleId,
            type:"POST",
            contentType: "application/json",
            async : false,
            success : function(resp){
                if ('success' === resp.code) {
                    $.App.notify(resp.message);
                    $.App.Page['RoleUserList'].draw();
                } else {
                    $.App.notify(resp.message, {type: "error"});
                }
            },
            error : function(){
                $.App.notify("连接失败,请稍候再试!", {type: "error"});
            }
        });
    });

    $("update-btn").on("click", function() {
        $.ajax({
            type: "POST",
            url:"/role/save",
            data: JSON.stringify($('#updateRoleForm').serializeObject()),
            contentType: "application/json",
            dataType: "json",
            async: false,
            error: function(request) {
                $.App.notify('连接失败,请稍候再试!', {type: "error"});
            },
            success: function(resp) {
                if ('success' === resp.code) {
                    $.App.notify(resp.message);
                } else {
                    $.App.notify(resp.message, {type: "error"});
                }
            }
        });
    });

    $("#search-perm-btn").on("click", function() {
        var searchText = $("#search-perm-text").val();
        $.App.Page["RolePermSelectList"].columns(2).search(searchText);
        $.App.Page["RolePermSelectList"].draw();
    });
    $("#search-user-btn").on("click", function() {
        var searchText = $("#search-user-text").val();
        $.App.Page["RoleUserSelectList"].columns(3).search(searchText);
        $.App.Page["RoleUserSelectList"].draw();
    });
});