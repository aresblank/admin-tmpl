$.App.Page.UserList = {
    "columns" : [
        { "data": "chk", "title": '<input type="checkbox" name="select_all" value="1" id="tb-select-all"></input>', "orderable": false  },
        { "data": "id", "title": "ID", "orderable": true  },
        { "data": "phone", "title": "手机号", "orderable": true },
        { "data": "email", "title": "邮箱", "orderable": false},
        { "data": "name", "title": "姓名", "orderable": false },
        { "data": "status", "title": "状态", "orderable": false },
        { "data": "opt", "title": "操作", "orderable": false }
    ],

    "columnDefs" : [
        {
            "targets": [0], // 目标列位置，下标从0开始
            'className': 'dt-center',
            "width" : 10,
            "render": function(data, type, row) { // 返回自定义内容
                return '<input type="checkbox" name="row-chk" value="' + row.id + '" />';
            }
        },
        {
            "targets": [-1], // 目标列位置，下标从0开始
            'className': 'dt-center',
            "width" : 80,
            "render": function(data, type, row) { // 返回自定义内容
               return '<a class="btn btn-xs btn-info" href="#/user/' + row.id + '.html">编辑</a>' +
                      '<a class="btn btn-xs btn-danger btn-row-remove" href="javascript:void(0);" data-id="' + row.id +'">删除</a>';
            }
        }
    ],
    "drawCallback" : function(settings) {
        $('#UserList input[type=checkbox]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '10%'
        });
    }
};

$(document).ready(function() {
    //查询
    $(".query_btn").on('click', function(){
        $.App.Page['UserList'].columns(2).search($("#phone").val());
        $.App.Page['UserList'].columns(4).search($("#name").val());
        $.App.Page['UserList'].draw();
    });
    //全选
    $("#UserList").on("ifChecked", "#tb-select-all", function(event){
        $("input[type=checkbox][name=row-chk]").iCheck('check');
    });
    $("#UserList").on("ifUnchecked", "#tb-select-all", function(event){
        $("input[type=checkbox][name=row-chk]").iCheck('uncheck');
    });

     $("#UserList").on("ifChecked", "input[type=checkbox][name=row-chk]", function() {
        var checkItemSize = $("input[type=checkbox][name=row-chk]:checked").length;
        console.log(checkItemSize);
        if ( checkItemSize > 0) {
            $(".del_btn").removeClass("hide");
        }
     });

     $("#UserList").on("ifUnchecked", "input[type=checkbox][name=row-chk]", function() {
        var checkItemSize = $("input[type=checkbox][name=row-chk]:checked").length;
        console.log(checkItemSize);
        if ( checkItemSize == 0) {
            $(".del_btn").addClass("hide");
            $("#tb-select-all").iCheck('uncheck');
        }
     });

    //打开新建用户弹窗
    $(".add_btn").on("click", function() {
        $("#addModal").modal("show");
    });

    //保存用户
    $(".add_btn_save").on("click", function() {
        $.ajax({
            type: "POST",
            url:"/user/save",
            data: JSON.stringify($('#addUserForm').serializeObject()),
            contentType: "application/json",
            dataType: "json",
            async: false,
            error: function(request) {
                $.App.notify('连接失败,请稍候再试!', {type: "error"});
            },
            success: function(resp) {
                if ('success' === resp.code) {
                    $("#addModal").modal("hide");
                    $.App.notify(resp.message);
                    setTimeout(function() {
                        $.App.goPage('/user/' + resp.data.userId + ".html");
                    }, 1000);
                } else {
                    $.App.notify(resp.message, {type: "error"});
                }
            }
        });
    });

    $(".del_btn").on("click", function() {
        var chkItems = $("input[type=checkbox][name=row-chk]:checked");
        if (chkItems.length > 0) {
            var ids = [];
            for (i = 0 ;i < chkItems.length; i++) {
                ids[i] = $(chkItems[i]).val();
            }
            deleteUser(ids);
        }
    });

    $("#UserList").on("click", ".btn-row-remove", function() {
        var userId = $(this).data("id");
        deleteUser([userId]);
    });

    function deleteUser(userIds) {
        $.App.alert('警告', '确定要删除吗?', function(){
            $.ajax({
                url:"/user/delete",
                type:"POST",
                data : JSON.stringify(userIds),
                contentType: "application/json",
                async : false,
                success : function(resp){
                    if ('success' === resp.code) {
                        $.App.notify(resp.message);
                        $.App.Page['UserList'].draw();
                    } else {
                        $.App.notify(resp.message, {type: "error"});
                    }
                },
                error : function(){
                    $.App.notify("连接失败,请稍候再试!", {type: "error"});
                }
            });
        });
    }
});
