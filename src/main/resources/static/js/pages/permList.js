$.App.Page.PermList = {
    "columns" : [
        { "data": "chk", "title": '<input type="checkbox" name="select_all" value="1" id="tb-select-all"></input>', "orderable": false  },
        { "data": "id", "title": "ID", "orderable": true  },
        { "data": "type", "title": "类型", "orderable": false },
        { "data": "name", "title": "名称", "orderable": true},
        { "data": "info", "title": "描述", "orderable": false },
        { "data": "opt", "title": "操作", "orderable": false }

    ],

    "columnDefs" : [
        {
            "targets": [0], // 目标列位置，下标从0开始
            'className': 'dt-center',
            "width" : 10,
            "render": function(data, type, row) { // 返回自定义内容
                return '<input type="checkbox" name="row-chk" value="' + row.id + '" />';
            }
        },
        {
            "targets": [-1], // 目标列位置，下标从0开始
            'className': 'dt-center',
            "width" : 80,
            "render": function(data, type, row) { // 返回自定义内容
               return '<a class="btn btn-xs btn-info" href="#/perm/' + row.id + '.html">编辑</a>' +
                      '<a class="btn btn-xs btn-danger btn-row-remove" href="javascript:void(0);" data-id="' + row.id +'">删除</a>';
            }
        }
    ],
    "drawCallback" : function(settings) {
        $('#PermList input[type=checkbox]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '10%'
        });
    }
};

$(document).ready(function() {
    //打开新建用户弹窗
    $(".add_btn").on("click", function() {
        $("#addModal").modal("show");
    });
    $(".add_btn_save").on("click", function(){
        $.ajax({
            type: "POST",
            url:"/perm/save",
            data: JSON.stringify($('#addPermForm').serializeObject()),
            contentType: "application/json",
            dataType: "json",
            async: false,
            error: function(request) {
                $.App.notify('连接失败,请稍候再试!', {type: "error"});
            },
            success: function(resp) {
                if ('success' === resp.code) {
                    $("#addModal").modal("hide");
                    $.App.notify(resp.message);
                    setTimeout(function() {
                        $.App.goPage('/perm/' + resp.data.permId + ".html");
                    }, 1000);
                } else {
                    $.App.notify(resp.message, {type: "error"});
                }
            }
        });
    });

    $("#PermList").on("click", ".btn-row-remove", function() {
        var permId = $(this).data("id");
        deletePerm([permId]);
    });

    function deletePerm(permIds) {
        $.App.alert('警告', '确定要删除吗?', function(){
            $.ajax({
                url:"/perm/delete",
                type:"POST",
                data : JSON.stringify(permIds),
                contentType: "application/json",
                async : false,
                success : function(resp){
                    if ('success' === resp.code) {
                        $.App.notify(resp.message);
                        $.App.Page['PermList'].draw();
                    } else {
                        $.App.notify(resp.message, {type: "error"});
                    }
                },
                error : function(){
                    $.App.notify("连接失败,请稍候再试!", {type: "error"});
                }
            });
        });
    }

    //全选
    $("#PermList").on("ifChecked", "#tb-select-all", function(event){
        $("input[type=checkbox][name=row-chk]").iCheck('check');
    });
    $("#PermList").on("ifUnchecked", "#tb-select-all", function(event){
        $("input[type=checkbox][name=row-chk]").iCheck('uncheck');
    });

     $("#PermList").on("ifChecked", "input[type=checkbox][name=row-chk]", function() {
        var checkItemSize = $("input[type=checkbox][name=row-chk]:checked").length;
        console.log(checkItemSize);
        if ( checkItemSize > 0) {
            $(".del_btn").removeClass("hide");
        }
     });

     $("#PermList").on("ifUnchecked", "input[type=checkbox][name=row-chk]", function() {
        var checkItemSize = $("input[type=checkbox][name=row-chk]:checked").length;
        console.log(checkItemSize);
        if ( checkItemSize == 0) {
            $(".del_btn").addClass("hide");
            $("#tb-select-all").iCheck('uncheck');
        }
     });

     $(".del_btn").on("click", function() {
         var chkItems = $("input[type=checkbox][name=row-chk]:checked");
         if (chkItems.length > 0) {
             var ids = [];
             for (i = 0 ;i < chkItems.length; i++) {
                 ids[i] = $(chkItems[i]).val();
             }
             deletePerm(ids);
         }
     });

     $("#search-btn").on("click", function() {
         $.App.Page['PermList'].columns(3).search($("#search-text").val());
         $.App.Page['PermList'].draw();
     });
});
