$.App.Page.RoleList = {
    "columns" : [
        { "data": "id", "title": "ID", "orderable": false  },
        { "data": "name", "title": "名称", "orderable": true },
        { "data": "info", "title": "描述", "orderable": false},
        { "data": "opt", "title": "操作", "orderable": false }
    ],

    "columnDefs" : [
        {
            "targets": [-1], // 目标列位置，下标从0开始
            'className': 'dt-center',
            "width" : 80,
            "render": function(data, type, row) { // 返回自定义内容
               return '<a class="btn btn-xs btn-info" href="#/role/' + row.id + '.html">编辑</a>' +
                      '<a class="btn btn-xs btn-danger btn-row-remove" href="javascript:void(0);" data-id="' + row.id +'">删除</a>';
            }
        }
    ],
    "drawCallback" : function(settings) {
        $('#UserList input[type=checkbox]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '10%'
        });
    }
};

$(document).ready(function() {
    $("#search-btn").on("click", function() {
        $.App.Page['RoleList'].columns(1).search($("#search-text").val());
        $.App.Page['RoleList'].draw();
    });
    //打开新建用户弹窗
    $(".add_btn").on("click", function() {
        $("#addModal").modal("show");
    });

    $(".add_btn_save").on("click", function(){
        $.ajax({
            type: "POST",
            url:"/role/save",
            data: JSON.stringify($('#addRoleForm').serializeObject()),
            contentType: "application/json",
            dataType: "json",
            async: false,
            error: function(request) {
                $.App.notify('连接失败,请稍候再试!', {type: "error"});
            },
            success: function(resp) {
                if ('success' === resp.code) {
                    $("#addModal").modal("hide");
                    $.App.notify(resp.message);
                    setTimeout(function() {
                        $.App.goPage('/role/' + resp.data.roleId + ".html");
                    }, 1000);
                } else {
                    $.App.notify(resp.message, {type: "error"});
                }
            }
        });
    });

    $("#RoleList").on("click", ".btn-row-remove", function() {
        var userId = $(this).data("id");
        deleteRole([userId]);
    });

    function deleteRole(userIds) {
        $.App.alert('警告', '确定要删除吗?', function(){
            $.ajax({
                url:"/role/delete",
                type:"POST",
                data : JSON.stringify(userIds),
                contentType: "application/json",
                async : false,
                success : function(resp){
                    if ('success' === resp.code) {
                        $.App.notify(resp.message);
                        $.App.Page['RoleList'].draw();
                    } else {
                        $.App.notify(resp.message, {type: "error"});
                    }
                },
                error : function(){
                    $.App.notify("连接失败,请稍候再试!", {type: "error"});
                }
            });
        });
    }
});
